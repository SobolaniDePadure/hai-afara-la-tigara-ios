//
//  ViewHaltController.swift
//  HALT
//
//  Created by Ioan Avram on 06.01.2022.
//

import UIKit
import Firebase
class ViewHaltController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    var ref: DatabaseReference!
    let uid=Auth.auth().currentUser?.uid
    var FrList: [String:AnyObject]=[:]
    var FrListNames: [String:String]=[:]

    @IBOutlet var FTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.FriendsList()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            for each in self.FrList
            {
                self.GetNameFromUid(Uid:each.key)
            }
        }
        FTable.delegate=self
        FTable.dataSource=self
        
        // Do any additional setup after loading the view.
    }
    @IBAction func HaltFriends(_ sender: Any) {
        FrList=[:]
        FrListNames=[:]
        self.FriendsList()
   
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            for each in self.FrList
            {
                self.GetNameFromUid(Uid:each.key)
            }
        }
    }
    
    @objc func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FrListNames.count
    }
    
    @objc(tableView:cellForRowAtIndexPath:) func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=FTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if(FrListNames.count>0){
        let index = FrListNames.index(FrListNames.startIndex, offsetBy: indexPath.row)
     
        cell.textLabel?.text=FrListNames.values[index]
        }

        return cell
    }
    func FriendsList() {
        ref = Database.database().reference()
        let _users = ref.child("Users")
        let _MyUserData = _users.child(self.uid!)
        let _MyUserFriends = _MyUserData.child("Friends")
        
        let _UsrFrQuery=_MyUserFriends.queryOrderedByKey()
        _UsrFrQuery.observeSingleEvent(of: .value)
        { (snapshot) in
            
            self.FrList = snapshot.value as! [String:AnyObject]
            print(self.FrList)
           

        }
   
    }
    func GetNameFromUid(Uid : String){
        
        ref = Database.database().reference()
        let _users = ref.child("Users")
        let _UsrQuery=_users.queryOrderedByKey()
            IsAvalable(usrQuery: _UsrQuery, Uid: Uid)
    }
    func IsAvalable(usrQuery : DatabaseQuery, Uid : String){
  
        
        usrQuery.observeSingleEvent(of: .value)
        {   (snapshot) in
            
            let datalist = snapshot.value as! [String:AnyObject]
            
            for each in datalist{
                if(each.key == Uid) {
                   let result=each.value.value(forKey: "smoking_available") as! Bool
                    print(result)
                    if(result==true)
                    {
                        let result2=each.value.value(forKey: "username") as! String
                         print(result2)
                         self.FrListNames[Uid]=result2
                         self.FTable.reloadData()
                    }
                    
                    
                }
            }
         
           
        }
            
        
        
    
    }


}
