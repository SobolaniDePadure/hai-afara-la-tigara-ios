//
//  ViewController.swift
//  HALT
//
//  Created by Ioan Avram on 15.10.2021.
//

import UIKit
import Firebase
class ViewController: UIViewController {
    @IBOutlet var picker:UIPickerView!
    @IBOutlet var savelbl:UILabel!
    @IBOutlet var smkswitch: UISwitch!
    var pickerData=["Winston","Marlboro","Kent","Other"]
    var SelectedData="Winston";
    var smoke_avalable=false ;
    var ref: DatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view

        picker.dataSource=self
        picker.delegate=self
        savelbl.text=" ";
        ref = Database.database().reference()
        let uid=Auth.auth().currentUser?.uid
        ref.child("Users").child(uid!).child("smoking_available").getData(completion:  { error, snapshot in
            guard error == nil else {
              print(error!.localizedDescription)
              return;
            }
            self.smoke_avalable = (snapshot.value as? Bool)!
            self.smkswitch.setOn(self.smoke_avalable, animated: true)
          });
        
    }
    @IBAction func SaveTouch(_ sender: Any) {
        
        print(SelectedData+" Saved");
        savelbl.text="Saved!";
         
    }
    @IBAction func Disconect(_ sender: Any) {
        do {
        try   Auth.auth().signOut();
    } catch let signOutError as NSError {
        print("Error signing out: %@", signOutError)}
      
    }
    @IBAction func smokswitch(_ sender: Any) {
        let uid=Auth.auth().currentUser?.uid as! String
        ref.child("Users").child(uid).child("smoking_available").setValue(smkswitch.isOn)
    }
    

}
extension ViewController:UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count;
    }
    
}
extension ViewController:UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row];
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

            SelectedData = pickerData[row] as String
        
     }

}
