//
//  FriendsController.swift
//  HALT
//
//  Created by Ioan Avram on 29.10.2021.
//

import UIKit
import Firebase
class FriendsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    var ref: DatabaseReference!
    let uid=Auth.auth().currentUser?.uid
    var FrList: [String:AnyObject]=[:]
    var FrListNames: [String:String]=[:]

    @IBOutlet var FTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.FriendsList()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            for each in self.FrList
            {
                self.GetNameFromUid(Uid:each.key)
            }
        }
        FTable.delegate=self
        FTable.dataSource=self
        
        // Do any additional setup after loading the view.
   
    }
 

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=FTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if(FrListNames.count>0){
        let index = FrListNames.index(FrListNames.startIndex, offsetBy: indexPath.row)
     
        cell.textLabel?.text=FrListNames.values[index]
        }

        return cell
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Halt")
        print(indexPath.row)
        self.FTable.deselectRow(at: indexPath, animated: true)
    }
    
    func FriendsList() {
        ref = Database.database().reference()
        let _users = ref.child("Users")
        let _MyUserData = _users.child(self.uid!)
        let _MyUserFriends = _MyUserData.child("Friends")
        
        let _UsrFrQuery=_MyUserFriends.queryOrderedByKey()
        _UsrFrQuery.observeSingleEvent(of: .value)
        { (snapshot) in
            
            self.FrList = snapshot.value as! [String:AnyObject]
            print(self.FrList)
           

        }
   
    }
    func GetNameFromUid(Uid : String){
        
        ref = Database.database().reference()
        let _users = ref.child("Users")
        let _UsrQuery=_users.queryOrderedByKey()
        _UsrQuery.observeSingleEvent(of: .value)
        { (snapshot) in
            
            let datalist = snapshot.value as! [String:AnyObject]
            
            for each in datalist{
                if(each.key == Uid) {
                   let result=each.value.value(forKey: "username") as! String
                    print(result)
                    self.FrListNames[Uid]=result
                    self.FTable.reloadData()
                    
                }
            }
         
           
        }
        
    }
    @IBAction func ReloadBtn(_ sender: Any) {
        FrList=[:]
        FrListNames=[:]
        self.FriendsList()
   
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            for each in self.FrList
            {
                self.GetNameFromUid(Uid:each.key)
            }
        }
    }
}

   
