//
//  AddFriendViewController.swift
//  HALT
//
//  Created by Ioan Avram on 05.11.2021.
//

import UIKit
import Firebase
class AddFriendViewController: UIViewController {
    var ref: DatabaseReference!
    @IBOutlet weak var FrAddedLabel: UILabel!
    @IBOutlet var pFriendtb: UITextField!
     var ResultUid=" ";
    override func viewDidLoad() {
        super.viewDidLoad()
        FrAddedLabel.text=""
        // Do any additional setup after loading the view.
    }
    let uid=Auth.auth().currentUser?.uid
    @IBAction func addFr(_ sender: Any)  {
        ref = Database.database().reference()
            
       // ref.child("Users").child(uid!).child("Friends").child(pFriendtb.text!).setValue("true");
        FindFriend(pFriendText: pFriendtb.text!);
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            print("ResultUid:" + self.ResultUid);
            if(self.ResultUid != " "){
            self.ref.child("Users").child(self.uid!).child("Friends").child(self.ResultUid).setValue(true);
                self.FrAddedLabel.text="Friend Added!"
            }
        }
        
        
        
    }
 
    func FindFriend(pFriendText:String) {
        ref = Database.database().reference()
        let _users = ref.child("Users")
        let _usersQuery=_users.queryOrderedByKey()
        _usersQuery.observeSingleEvent(of: .value){
            (snapshots) in for child in snapshots.children{
            
                print("\n-----------------------------------\n")
                let childData=child as! DataSnapshot
                let FrUid = childData.key as String
             //   print( childData.key)// the uid
                if let snap = childData.value as? [String:AnyObject]{
                    for each in snap{
                        if(each.key == "email")
                        {
                            let _email=each.value as! String
                            print("email of " + FrUid + " is " + _email)
                            if(pFriendText == _email)
                            {
                                //return FrUid
                                self.ResultUid = FrUid
                                
                            }
                            
                        }
                    }
                    
                }
                
            }
        }
    
    }

}
