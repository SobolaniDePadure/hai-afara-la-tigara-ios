//
//  LogInController.swift
//  HALT
//
//  Created by Ioan Avram on 28.10.2021.
//

import UIKit
import Firebase
class LogInController: UIViewController {

    @IBOutlet var passtf: UITextField!
    @IBOutlet var emailtf: UITextField!
    @IBOutlet var wrongLBL: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Do any additional setup after loading the view.
        wrongLBL.text=""
    }
   
    @IBAction func signuppressed(_ sender: Any) {
       guard let email=emailtf.text else {return}
        guard let password=passtf.text else { return }
        Firebase.Auth.auth().createUser(withEmail: email,
                                        password: password,
                                        completion:
                                            { [self] result,error in guard error == nil else
                                            {
            print("signup Wrong!")
            wrongLBL.text="allready taken"
            return
        }
            wrongLBL.text="Now you can press login"
            var ref: DatabaseReference!
            var usrname=""
            let uid=Auth.auth().currentUser?.uid
            for char in email {
                
                if(char=="@")
                {
                    break
                }
                else
                {
                    usrname.append(char)
                }
            }
            
            ref = Database.database().reference()

            ref.child("Users").child(uid!).child("username").setValue(usrname)
            ref.child("Users").child(uid!).child("email").setValue(email)
            ref.child("Users").child(uid!).child("smoking_avalable").setValue(true)

            print("SignUp Good!")
        })
        
    }
    
    @IBAction func loginpressed(_ sender: Any) {
        let email=emailtf.text
        guard let password=passtf.text else { return }
        Firebase.Auth.auth().signIn(withEmail: email!, password: password, completion: { [self] result,error in guard error == nil else{
            print("LogIn Wrong!")
            wrongLBL.text="Wrong username or Password"
            return
        }
            let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "tbar") as! UITabBarController
            self.present(nextvc, animated: true,completion:nil);
            
            print("LogIn Good!")
        })
    }
}

